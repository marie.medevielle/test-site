---
title: La fiche du cours
layout: default
categorie: seconde
author: Mawe
---
On s en fou

{% assign pages = site.pages | where_exp: 'page', 'page.title' %}
{% for page in pages %}
    title: {{page.url}}
{% endfor %}

<ul>
{% assign sitepages = site.pages | sort: 'order' %}
{% for sitepage in sitepages %}
  <li {% if page.url == sitepage.url %} class="active"{% endif %}>
    <a href="{{ sitepage.url }}">{{ sitepage.title }}</a>
  </li>
{% endfor %}
</ul>